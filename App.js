import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import DriverDetail from './screen/DriverDetail'
import Dashbaord from './screen/Dashboard'
const Stack = createStackNavigator();
function MyStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator
      screenOptions={{headerShown:false}}>
      <Stack.Screen name="Dashboard" component={Dashbaord} />
      <Stack.Screen name="DriverDetail" component={DriverDetail} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default MyStack;