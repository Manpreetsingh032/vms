import React from 'react';
import {ToastAndroid,} from 'react-native';

const showToastWithGravity = (message) => {
    ToastAndroid.showWithGravity(
        message,
        ToastAndroid.SHORT,
        ToastAndroid.CENTER
    );
};

export default showToastWithGravity;
