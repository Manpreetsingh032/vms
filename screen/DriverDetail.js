import React, { useState, useEffect } from 'react';
import {
    View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, TextInput, StatusBar, ActivityIndicator
} from 'react-native';

import DateTimePicker from '@react-native-community/datetimepicker';
import LinearGradient from 'react-native-linear-gradient';
import Feather from 'react-native-vector-icons/Feather';
import showToastWithGravity from '../../VehicleManagement/components/toast';
export default function DriverDetail({ navigation }) {
    const [loading, setLoading] = useState(false);
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [data, setData] = useState({
        fName: '',
        lName: '',
        birthDate: '',
        truck: '',
        rc: '',
        drivingLicense: '',
        address: '',
        mobileNumber: '',
        isValidUser: true,
        isValidPassword: true,
    })
    const onfNameChange = (val) => {
        setData({
            ...data,
            fName: val,
        })
    }
    const onlNameChange = (val) => {
        setData({
            ...data,
            lName: val,
        })
    }
    const onMobileChange = (val) => {
        setData({
            ...data,
            mobileNumber: val,
        })
    }
    const onAddressChange = (val) => {
        setData({
            ...data,
            address: val,
        })
    }
    const onLicenseChange = (val) => {
        setData({
            ...data,
            drivingLicense: val,
        })
    }
    const onTruckChange = (val) => {
        setData({
            ...data,
            truck: val,
        })
    }
    const onRcChange = (val) => {
        setData({
            ...data,
            rc: val,
        })
    }


    const onChange = (event, selectedDate) => {
        let currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        let vari = currentDate.toString().split(" ");
        let val = vari[2] + " " + vari[1] + "  " + vari[3];
        console.log(val);
        setData({
            ...data,
            birthDate: val
        });
    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    async function submitStudentDetails()
    {
        setLoading(!loading)
      if(data.studentName!="" && data.roll!="" && data.class!=""&&data.birthDate!=""&&
          data.schoolName!="" && data.firstGuardianName!=""&&data.firstGuardianName.length>3 &&data.secondGuardianName!=""
          &&data.firstGuardianOccupation!="" && data.secondGuardianOccupation!="" && data.address!=""
          && data.firstGuardianName!="" && data.mobileNumber!=""){

              if(data.mobileNumber.length==10){
      }
  else{
      setLoading(loading);
      showToastWithGravity("Enter Correct mobile Number");
  }
  }
      else{
          setLoading(loading);
          alert("All the fields are mandatory and First Guardian name must be 3 characters or long")
      }
        
    }

    return (
        <LinearGradient colors={['#434040', '#171716']}
            style={styles.container}>
            <StatusBar backgroundColor='#000' barStyle="light-content" />
            <View style={styles.header}>
                <Text style={[styles.heading, {
                }]}>Driver Details</Text>
            </View>
            <View style={styles.action} />

            <ScrollView>
                <Text style={[styles.text_footer, {
                }]}>Full Name</Text>
                <View style={styles.action}>

                    <TextInput
                        value={data.fName}
                        placeholder="Enter First Name"
                        placeholderTextColor="grey"
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(val) => onfNameChange(val)}

                    />
                </View>

                <Text style={[styles.text_footer, {
                }]}>Last Name</Text>
                <View style={styles.action}>

                    <TextInput
                        value={data.lName}
                        placeholder="Enter Last Name"
                        placeholderTextColor="grey"
                        style={styles.textInput}
                        keyboardType='number-pad'
                        autoCapitalize="none"
                        onChangeText={(val) => onlNameChange(val)}

                    />
                </View>

                <Text style={[styles.text_footer, {
                }]}>Age</Text>
                <View style={styles.action}>

                    <TextInput
                        value={data.birthDate}
                        placeholder="DD/MM/YY"
                        placeholderTextColor="grey"
                        style={styles.textInput}
                        autoCapitalize="none"
                    />

                    <Feather onPress={showDatepicker}
                        name="calendar"
                        color="green"
                        size={20}
                    />

                </View>

                {show ?
                    <DateTimePicker
                        testID="datepicker"
                        value={date}
                        mode={mode}
                        display="spinner"
                        onChange={onChange}
                    />
                    : null
                }


                <Text style={[styles.text_footer, {
                }]}>Mobile Number</Text>
                <View style={styles.action}>

                    <TextInput
                        value={data.mobileNumber}
                        textInput='number'
                        maxLength={10}
                        keyboardType='number-pad'
                        placeholder="Mobile Number"
                        placeholderTextColor="grey"
                        style={styles.textInput}
                        onChangeText={(val) => onMobileChange(val)}
                        autoCapitalize="none"

                    />
                </View>



                <Text style={[styles.text_footer, {
                }]}>Address</Text>
                <View style={styles.action}>

                    <TextInput
                        value={data.address}
                        placeholder="Address"
                        placeholderTextColor="grey"
                        style={styles.textInput}
                        onChangeText={(val) => onAddressChange(val)}
                        autoCapitalize="none"

                    />
                </View>

                <Text style={[styles.text_footer, {
                }]}>Driving License</Text>
                <View style={styles.action}>
                    <TextInput
                        value={data.drivingLicense}
                        onChangeText={(val) => onLicenseChange(val)}
                        placeholder="Enter Driving license"
                        placeholderTextColor="grey"
                        style={styles.textInput}
                        autoCapitalize="none"

                    />
                </View>


                <Text style={[styles.text_footer, {
                }]}>Truck No</Text>
                <View style={styles.action}>

                    <TextInput
                        value={data.truck}
                        placeholder="Enter Truck No"
                        placeholderTextColor="grey"
                        style={styles.textInput}
                        onChangeText={(val) => onTruckChange(val)}
                        autoCapitalize="none"

                    />
                </View>



                <Text style={[styles.text_footer, {
                }]}>RC No</Text>
                <View style={styles.action}>

                    <TextInput
                        value={data.rc}
                        placeholder="Enter RC No"
                        placeholderTextColor="grey"
                        style={styles.textInput}
                        onChangeText={(val) => onRcChange(val)}
                        autoCapitalize="none"

                    />
                </View>

                <View style={styles.button}>

                    <TouchableOpacity style={{ padding: 8 }} onPress={() => showToastWithGravity("Submitted Successfully")}>
                        <LinearGradient
                            colors={['#673AB7', '#512DA8']}
                            style={styles.buttonStyle}
                            onPress={() => navigation.navigate('Dashboard')}>
                            <Text style={{ fontWeight: 'normal', fontSize: 18, color: '#fff' }}>Submit</Text>
                        </LinearGradient>
                    </TouchableOpacity>

                </View>

            </ScrollView>

        </LinearGradient>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    heading: {
        marginTop: '3%',
        marginBottom: '1%',
        marginLeft: '10%',
        marginRight: '10%',
        fontWeight: 'bold',
        color: '#fff',
        fontSize: 25,


    },
    header: {
        alignItems: 'center',
    },

    footer: {
        flex: 1,
        flexDirection: 'column',
        padding: 14,
    },
    iconImage: {
        width: 300,
        height: 150,
        marginTop: '3%',
        marginBottom: '3%',
    },
    errorMsg: {
        marginStart: '10%',
        marginEnd: '10%',
        color: '#FF0000',
        fontSize: 14,
    },

    button: {
        alignItems: 'center',
        marginTop: '5%',
        marginBottom: '5%',
        padding: 8,
    },
    textStyle: {
        fontSize: 18,
        textAlign: 'left'
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        marginStart: '10%',
        marginEnd: '10%',
        borderBottomWidth: 1,
        borderBottomColor: '#fff',
        paddingBottom: 10
    },
    text_footer: {
        marginTop: '3%',
        marginBottom: '1%',
        marginLeft: '10%',
        marginRight: '10%',
        fontWeight: 'bold',
        color: '#fff',
        fontSize: 18

    },
    textInput: {
        color: '#fff',
        flex: 1,

    },

    buttonStyle: {
        width: 250,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
    }
    , loader: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }

})