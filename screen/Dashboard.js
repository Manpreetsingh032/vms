import React from 'react';
import {
    View, Text, Image,
    StyleSheet, TextInput, Button,
    SafeAreaView,
    TouchableOpacity, StatusBar, ScrollView
}
    from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import ToolbarAndroid from '@react-native-community/toolbar-android'
import Feather from 'react-native-vector-icons/Feather'
export default function Dashboard({ navigation }) {
    return (
        <LinearGradient colors={['#434040', '#171716']} style={styles.container}>
            <View style={styles.header}>
                <Feather style={{ marginStart: 5, alignItems: 'center' }}
                    name="home"
                    color="black"
                    size={20}
                />
                <Text style={[styles.heading]}>Carrier Service</Text>

                <TouchableOpacity
                    onPress={() => navigation.navigate('DriverDetail')}>
                    <Text style={[styles.heading]}>New Registration</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => navigation.navigate('DriverDetail')}>
                    <Text style={[styles.heading]}>Search Driver Details</Text>
                </TouchableOpacity>
            </View>

        </LinearGradient>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    header: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        width: '100%',
        height: '7%',
        alignItems: 'center'
    },

    heading: {
        alignItems: 'center',
        marginTop: '1%',
        fontWeight: 'bold',
        color: '#000',
        marginRight: '1%',
        marginLeft: '1%',
        fontSize: 14
    }
});
